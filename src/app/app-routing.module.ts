import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarEstudiantesComponent } from './Estudiante/listar/listar.component';
import { ListarProfesoresComponet } from './Profesores/listar/listar.component';


const routes: Routes = [
  {path:'listar-estudiante', component:ListarEstudiantesComponent},
  {path:'listar-profesor', component:ListarProfesoresComponet},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
