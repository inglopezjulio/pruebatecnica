export interface Profesor{
    id?: Number;
    nombres?: string;
    apellidos?: string;
    edad?: Number;
    identificacion?: Number;
    asignatura?: string;

}