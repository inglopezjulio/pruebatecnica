import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ListarEstudiantesComponent } from './Estudiante/listar/listar.component';
import { ListarProfesoresComponet } from './Profesores/listar/listar.component';
import { GuardarEstudianteComponent } from './Estudiante/guardar/guardar.component';
import { GuardarProfesorComponent } from './Profesores/guardar/guardar.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarEstudiantesComponent,
    ListarProfesoresComponet,
    GuardarEstudianteComponent,
    GuardarProfesorComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
