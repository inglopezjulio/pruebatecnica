import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Estudiante } from '../Model/Estudiante';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  constructor(private http:HttpClient){}

  Url='http://localhost:8080/estudiante';

  getAll(){
    return this.http.get<Estudiante[]>(this.Url)
  }

  crear(estudiante:Estudiante){
    return this.http.post<Estudiante>(this.Url,estudiante)
  }
}
