import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profesor } from '../Model/Profesor';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  constructor(private http:HttpClient){}

  Url='http://localhost:8080/profesor';

  getAll(){
    return this.http.get<Profesor[]>(this.Url)
  }

  crear(profesor:Profesor){
    return this.http.post<Profesor>(this.Url,profesor)
  }
}
