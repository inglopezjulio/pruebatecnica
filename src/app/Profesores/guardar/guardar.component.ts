import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Profesor } from 'src/app/Model/Profesor';
import { ProfesorService } from 'src/app/Service/profesor.service';

@Component({
  selector: 'app-guardar',
  templateUrl: './guardar.component.html',
  styleUrls: ['./guardar.component.css']
})
export class GuardarProfesorComponent implements OnInit {

  public profesor: Profesor = {};

  @Output()
  public emmiter = new EventEmitter();

  constructor(public activeModal: NgbActiveModal, public profesorService: ProfesorService) { }

  ngOnInit(): void {

  }

  CerrarModal() {
    this.activeModal.close()
  }

  Guardar() {
    this.profesorService.crear(this.profesor)
      .subscribe(data => {
        this.activeModal.close();
        this.emmiter.emit();
        alert("¡Profesor creado con exito!");
      })
  }

}
