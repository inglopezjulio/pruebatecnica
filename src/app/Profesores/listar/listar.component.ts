import { Component, NgModule, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Profesor } from 'src/app/Model/Profesor';
import { ProfesorService } from 'src/app/Service/profesor.service';
import { GuardarProfesorComponent } from '../guardar/guardar.component';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarProfesoresComponet implements OnInit {

  public profesores: Profesor[] = [];
  constructor(private profesorService: ProfesorService, private modalservice: NgbModal) { }

  ngOnInit(): void {
    this.profesorService.getAll().subscribe(data => this.profesores = data);
  }

  abrirFormulario(): void {
    const modalRef = this.modalservice.open(GuardarProfesorComponent, {
      backdrop: 'static'
    });

    modalRef.componentInstance.emmiter.subscribe(() => this.ngOnInit())
  }

}
