import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PruebaAngular';

  constructor(private route:Router){}

  listarEstudiante(){
    this.route.navigate(["listar-estudiante"]);
  }

  listarProfesor(){
    this.route.navigate(["listar-profesor"]);
  }
}
