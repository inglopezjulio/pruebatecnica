import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Estudiante } from 'src/app/Model/Estudiante';
import { EstudianteService } from 'src/app/Service/estudiante.service';

@Component({
  selector: 'app-guardar',
  templateUrl: './guardar.component.html',
  styleUrls: ['./guardar.component.css']
})
export class GuardarEstudianteComponent implements OnInit {

  public estudiante: Estudiante = {};

  @Output()
  public emmiter = new EventEmitter();

  constructor(public activeModal: NgbActiveModal, public estudianteService: EstudianteService) { }

  ngOnInit(): void {

  }

  CerrarModal() {
    this.activeModal.close()
  }

  Guardar() {
    this.estudianteService.crear(this.estudiante)
      .subscribe(data => {
        this.activeModal.close();
        this.emmiter.emit();
        alert("¡Estudiante creado con exito!");
      })
  }

}
