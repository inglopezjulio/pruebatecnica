import { Component, NgModule, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Estudiante } from 'src/app/Model/Estudiante';
import { EstudianteService } from 'src/app/Service/estudiante.service';
import { GuardarEstudianteComponent } from '../guardar/guardar.component';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarEstudiantesComponent implements OnInit {

  public estudiantes: Estudiante[] = [];
  constructor(private estudianteService: EstudianteService, private modalservice: NgbModal) { }

  ngOnInit(): void {
    this.estudianteService.getAll().subscribe(data => this.estudiantes = data);
  }

  abrirFormulario(): void {
    const modalRef = this.modalservice.open(GuardarEstudianteComponent, {
      backdrop: 'static'
    });

    modalRef.componentInstance.emmiter.subscribe(() => this.ngOnInit())
  }

}
